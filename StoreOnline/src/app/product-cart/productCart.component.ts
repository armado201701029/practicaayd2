import {Component, OnInit} from '@angular/core';
import { CartService } from './productCart.service';
import { FormBuilder } from '@angular/forms';
declare var $: any;
@Component({
  selector: 'app-product-cart',
  templateUrl: './productCart.component.html',
  styleUrls: [ './productCart.component.css' ]
})

export class ProductCartComponent implements OnInit{
  items;
  css :string="";
  total = 0;
  products;
  constructor(
    private cartService: CartService
  ) {
  }
  ngOnInit(): void {
  }
  delete(id){
    this.cartService.deleteItem(id);
    this.total = 0;
    this.ngOnInit();
  }
  comprar(){
    const pila: [] = JSON.parse(localStorage.getItem('pila'));
    this.products = JSON.stringify(pila);
    alert('Su pdido fue enviado exitosamente ' + this.products);
  }
}
