import random
import string
import time


#SELENIUM_HUB = 'http://172.17.0.2:4444/wd/hub'
SELENIUM_HUB = 'http://localhost:4444/wd/hub'

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

driver = webdriver
driver = webdriver.Remote(
    command_executor=SELENIUM_HUB,
    desired_capabilities=DesiredCapabilities.FIREFOX,
)

def generador_txt(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
#CARNET 201701029
PATH_BASE = 'http://localhost:4200/'
try:
    
    #Resgistro de Usuario
    urlprueba = PATH_BASE + 'register'
    driver.get('http://35.224.25.184:4200/register')
    time.sleep(8)
    print("se abrio la pagina")
    search_e = driver.find_element_by_id("nombre")
    search_e.clear()
    search_e.send_keys("edson")

    
    search_t = driver.find_element_by_id("email")
    search_t.clear()
    search_t.send_keys("edson@gmail.com")

    search_t = driver.find_element_by_id("password")
    search_t.clear()
    search_t.send_keys("123")

    
    search_btn = driver.find_element_by_id("registrarid")
    search_btn.click()
    time.sleep(1)
    assert "Funciona la prueba" not in driver.page_source
    print("1.   Prueba de Registro de Usuario Correcta")
    
    #Resgistro de Producto http://35.224.25.184/
    driver.get('http://35.224.25.184:4200/create_shoe/Edson_2020')
    time.sleep(8)
    print("se abrio la pagina")
    search_e = driver.find_element_by_id("codigo")
    search_e.clear()
    search_e.send_keys("23123aa")

    
    search_t = driver.find_element_by_id("nombre")
    search_t.clear()
    search_t.send_keys("tenis deportivos")

    search_t = driver.find_element_by_id("precio")
    search_t.clear()
    search_t.send_keys("200")

    search_t = driver.find_element_by_id("disponibilidad")
    search_t.clear()
    search_t.send_keys("si")

    search_t = driver.find_element_by_id("cantidad")
    search_t.clear()
    search_t.send_keys("123")

    search_t = driver.find_element_by_id("descripcion")
    search_t.clear()
    search_t.send_keys("calzado")

    
    search_btn = driver.find_element_by_id("crearproducto")
    search_btn.click()
    time.sleep(1)
    assert "Funciona la prueba" not in driver.page_source
    print("2.   Prueba de Registro de Producto Correcta")

except:
    print("Error en un prueba")
    assert "No results found."  in driver.page_source
finally:
    driver.quit()
    print("-- Cierra sesion del driver --")